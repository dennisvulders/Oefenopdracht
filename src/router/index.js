import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Upload from '@/views/Upload.vue'
import AddUser from '@/views/AddUser.vue';
import Details from '@/views/Details.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/details/:id/upload',
    name: 'Upload',
    component: Upload
  },
  {
    path: '/add-user',
    name: 'add-user',
    component: AddUser,
  },
  {
    path: '/details/:id',
    name: 'details',
    component: Details
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://dump.lwdev.nl/vue-cursus-api/deelnemers/`,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getEvents() {
    return apiClient.get('/deelnemers')
  },
  getEvent(id) {
    return apiClient.get('/deelnemers/' + id)
  }
}

// stappen hieronder waren nodig om het te laten werken
// npm install -g json-server  
// Set-ExecutionPolicy RemoteSigned
// json-server --watch db.json
// Set-ExecutionPolicy Restricted  (deze beveiligd het weer)